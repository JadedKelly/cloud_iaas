Demo Project:
Cretae server & deploy application on Digital Ocean

Tech used:
Digital Ocean, Linux, Java & Gradle

Project Description:
-> Setup and configure a server on Digital Ocean
-> Create & configure a new Linux user on the Droplet
-> Deploy & run a Java Gradle application on Droplet
